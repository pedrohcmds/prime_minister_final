import os


def get_ministro():
    return "Chamberlain"
def get_arquivos(ministro):
    
    arquivos = f"/home/labri_pedro/prime_minister/arquivos_limpos/{ministro}"
    return arquivos

def cria_tabela(ministro):
    tabela = f"nova_tabela_limpa_{ministro}.csv"
    with open (tabela, "w") as file:
        file.write(f"Local arquivo; Nome do Arquivo; Data; Camara; Título; Autor; Número de palavras; Fala; \n")
    
def escreve_na_tabela(nome, data, ministro, camara, titulo, local_arquivo, autor,  palavras, texto):
    tabela = f"nova_tabela_limpa_{ministro}.csv"
    with open (tabela, "a") as file:
        file.write(f"{local_arquivo};{nome};{data};{camara};{titulo};{autor};{palavras};{texto}; \n")
    print("Informações inseridas na tabela")
        
    
def main():
    cria_tabela(ministro=get_ministro())
    ministro = get_ministro()
    arquivos = get_arquivos(ministro)
    arquivos = os.listdir(arquivos)
    
    for i, arquivo in enumerate(arquivos):
        print(arquivo)
        nome = arquivo
        texto_escrever = []
        bruto = nome.split("-")
        data = bruto[0:3]
        data = "-".join(data)
       
        camara = bruto[4]
        
        if(len(bruto)) == 7:
            titulo = bruto[5]
            
        elif (len(bruto)) == 8:
            titulo = bruto[5:6]
            titulo = " ".join(titulo)

        elif (len(bruto)) == 9:
            titulo = bruto[5:7]
            titulo = " ".join(titulo)
        elif (len(bruto)) == 10:
            titulo = bruto[5:8]
            titulo = " ".join(titulo)
        else: 
            titulo = bruto[5:10]
            titulo = " ".join(titulo)
        print(titulo)
           

        autor = bruto[-1]
        autor = autor.strip(".txt")
        
        local_arquivo = get_arquivos(ministro) + "/" + nome
        local_arquivo_open = get_arquivos(ministro).replace("/","//") + "//" + nome
        with open(local_arquivo_open, "r") as file:
           texto = file.read()
           texto = texto.replace(";",".")
        palavras = len(texto.split(" "))
        texto_escrever.append(texto)
        escreve_na_tabela(nome, data, ministro, camara, titulo, local_arquivo, autor, palavras, texto_escrever)
        
if __name__ == '__main__':
    main()
