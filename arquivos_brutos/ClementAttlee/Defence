
<!DOCTYPE html>
<!--[if IE 7]> <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width" />
    
        <!-- Google Tag Manager -->
    <script>
    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
            new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NFVRG43');</script>
    <!-- End Google Tag Manager -->


    <meta name="description" content="Hansard (the Official Report) is the edited verbatim report of proceedings of both the House of Commons and the House of Lords. Daily Debates from Hansard are published on this website the next working day.">

    <link rel="dns-prefetch" href="//ajax.googleapis.com/" />
    <link rel="shortcut icon" href="/Assets/img/favicon.ico" type="image/x-icon" />

        <title>Defence - Hansard</title>
    <link href="/assets/css/bundle?v=iGtf8YJxXRNBeHXf1CZ6ukADmT4hYFCJq0nsETHFFb41" rel="stylesheet"/>

    <script src="/assets/js/modernizr/bundle?v=4Dmh6ovjssOKPw9dZqg0U8FgOCXLJMRqYqL4wv9UEJM1"></script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>

    <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NFVRG43"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<div id="bootstrap-datepicker-container"></div>
<div class="disclaimer" id="data-disclaimer">
    <div class="container">
        <div class="text">
            <strong>Cookies: </strong>We use cookies to give you the best possible experience on our site. By continuing to use the site you
            agree to our use of cookies. <a href="http://www.parliament.uk/site-information/privacy/">Find out more</a>
        </div>
        <button type="submit" class="btn btn-small">OK</button>
    </div>
</div>
<div id="wrapper">
    <header class="header header-secondary">
    <div class="container">
        <a href="https://www.parliament.uk" class="uk-parliament" target="_blank">
            UK Parliament
        </a>
        <div class="links hidden-xs hidden-sm">
            <a target="_blank" href="http://www.parliament.uk/business/">Parliamentary Business</a>
            <a target="_blank" href="http://www.parliament.uk/mps-lords-and-offices/">MPs, Lords and Offices</a>
            <a target="_blank" href="http://www.parliament.uk/about/">About Parliament</a>
            <a target="_blank" href="http://www.parliament.uk/get-involved/">Get Involved</a>
            <a target="_blank" href="http://www.parliament.uk/visiting/">Visit</a>
            <a target="_blank" href="http://www.parliament.uk/education/">Education</a>
        </div>
    </div>
</header>
<header class="header header-primary ">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="text">Menu</span>
                    <span class="close"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <a class="brand hidden-sm hidden-xs" href="/">
                    <div class="identity-text"><strong>Hansard</strong></div>
                </a>
                
                <a class="navbar-brand hidden-md hidden-lg" href="/">
                    <strong>Hansard</strong>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Commons <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/Commons/latestsittingday">Latest Sitting</a></li>
                            <li><a href="/Commons">Browse Sittings</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Debates?house=commons">Find Debates</a></li>
                            <li><a href="/search/Divisions?house=commons">Find Divisions</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Members?house=commons&amp;currentFormerFilter=1">Find MPs</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lords <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/Lords/latestsittingday">Latest Sitting</a></li>
                            <li><a href="/Lords">Browse Sittings</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Debates?house=lords">Find Debates</a></li>
                            <li><a href="/search/Divisions?house=lords">Find Divisions</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Members?house=lords&amp;currentFormerFilter=1">Find Peers</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/about?historic=false">Hansard Online</a></li>
                        </ul>
                    </li>
                </ul>

<form action="/search" class="navbar-form navbar-right" method="post" role="search">                    <div class="search-form">
                        <input id="SearchTermTop" name="SearchTerm" type="text" class="form-control" placeholder="Search Hansard...">
                        <button type="submit" class="btn"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
</form>            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    
    <div class="to-top-link">
        <div id="top-link-block" class="hidden">
            <a href="#" class="well well-sm hidden" id="previous-top-link"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a>
            <a href="#header" class="well well-sm" id="back-to-top-link"><i class="glyphicon glyphicon-chevron-up"></i> Top</a>
            <a href="#" class="well well-sm hidden" id="next-top-link"><i class="glyphicon glyphicon-chevron-right"></i> Next</a>
        </div>
    </div>

</header>


    <!-- Alert area always shown as they are added dynamically via JS -->
<div class="alert-area"></div>


    <div class="">
        <div id="main" class="content">
            

<!-- House coloured header bar -->
<div class="background-commons house-header col-xs-12">
    <div class="container">
        <span>House of Commons <strong>Hansard</strong></span>
        <div class="survey">
    <div class="message">
        <!-- Populated via JavaScript -->
    </div>
    <a href="#" class="close-button" title="Tap to hide survey banner">x</a>
</div>
    </div>
</div>

<div class="">
    

<div class="row content debate-content">
    <div class="col-xs-12">
        <!-- the breadcrumb is to navigate around the day and go back to the date picker-->
        <div class="breadcrumb-container breadcrumb-desktop hidden-xs">
            <div class="container">
                <ol class="breadcrumb">
                    <li>
                        <a href="/Commons/1942-10-22">
                            <strong>Contents</strong>
                        </a>
                    </li>
                        <li>
                                <a href="/Commons/1942-10-22/debates/ccf0e54f-a8ab-4b8f-ae97-fb3647815e84/CommonsChamber">
                                    <span class="commons">Commons Chamber</span>
                                </a>
                        </li>
                        <li>
                                <a href="/Commons/1942-10-22/debates/a7fabee7-df6f-4b8d-bcdf-0ec552eb0bcf/OralAnswersToQuestions">
                                    <span class="commons">Oral Answers To Questions</span>
                                </a>
                        </li>
                        <li>
                                <a href="/Commons/1942-10-22/debates/2aa3ec26-71da-4e28-8cb8-b42ae979041d/India">
                                    <span class="commons">India</span>
                                </a>
                        </li>

                    <li>
                        <div class="hidden-sm download-as-text">
                            <a class="link-text" href="/debates/GetDebateAsText/997781ce-9025-488c-ac93-d76051463276"><span class="glyphicon glyphicon-file"></span> Text only</a>
                        </div>

                    </li>
                </ol>
            </div>
        </div>

        <div class="breadcrumb breadcrumb-mobile visible-xs">
            <a class="btn btn-wide" href="/Commons/1942-10-22">
                Back to Contents
            </a>
        </div>

    </div>

    <div class="col-xs-12">
<div class="clearfix"></div>
<div class="alert-area">
    <div id="process-warning" class="alert-outer alert-hidden" data-processing-url="/processing/getcurrentlyprocessing">
    <div class="alert alert-info">
        <div class="alert-inner">
            <span class="glyphicon glyphicon-info-sign"></span>
            <span id="process-warning-message"></span>
        </div>
    </div>
</div>


            <div class="alert-outer">
                <div class="alert alert-info">
                    <div class="alert-inner">
                        <span class="glyphicon glyphicon-info-sign"></span>
                        The text on this page has been created from Hansard archive content, it may contain typographical errors.
                    </div>
                </div>
            </div>
</div>

    </div>

    <div class="col-xs-12 header">
        <div class="container">
            <div class="left-arrow">
                    <a href="/Commons/1942-10-22/debates/73b40a59-81ba-46dc-9d9f-74f9bc458a4c/ArmedForcesAndCivilians(PensionsAndGrants)" class="previous" title="Go to the previous debate">


                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
            </div>

            <div class="title">
                Defence
            </div>

            <div class="right-arrow">
                    <a href="/Commons/1942-10-22/debates/e7631782-47bf-4a03-abe6-52975bb17af0/PopulationStatistics" class="next" title="Go to the next debate">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
            </div>
        </div>

            <div class="col-xs-12">
                <div class="actions">
                    <div class="share-debate">
                        <a href="#debate-1603545" rel="popover" class="link-to-contribution link-text" title="Share this debate" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence">
                            <div class="share-icon">&nbsp;</div> Share
                        </a>
                    </div>
                </div>
            </div>


</div>


    <div class="col-xs-12 debate-date">
        <strong>22 October 1942</strong>
    </div>

    <div class="col-xs-12 debate-date">Volume 383</div>

    <div class="col-xs-12 col-md-12">
        <div class="container">
            <div id="debateContent">
                <div class="highlightedSearchTerms"></div>
                <div class="content-container">


<div class="content-item other-content">
        <!-- START statement -->
        <div class="statement col-md-9  content-container " id='contribution-f4980cea-808b-46a2-8014-2dbe794d6f38'>
            
<p class="">45.</p>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            </div>
        <div class="clearfix"></div>
        <!-- END statement -->
</div><div class="content-item" id="contribution-0a76d060-0c03-4eaf-ae95-afd3262c982b">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-0a76d060-0c03-4eaf-ae95-afd3262c982b">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-0a76d060-0c03-4eaf-ae95-afd3262c982b">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-0a76d060-0c03-4eaf-ae95-afd3262c982b">
                Mr. Granville
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-0a76d060-0c03-4eaf-ae95-afd3262c982b" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-0a76d060-0c03-4eaf-ae95-afd3262c982b">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">asked the Prime Minister whether he will consider the suggestion for the setting-up of an Allied War Council in India, inviting representatives of the United States of America, Russia and China to serve with those of Great Britain and India, in order to secure full strategic co-operation under the unified command of General Wavell of those powers concerned in the defence of India?</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-0a76d060-0c03-4eaf-ae95-afd3262c982b" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-0a76d060-0c03-4eaf-ae95-afd3262c982b">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-2b722924-64d3-4ac8-81cd-389c0281e59b">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-2b722924-64d3-4ac8-81cd-389c0281e59b">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-2b722924-64d3-4ac8-81cd-389c0281e59b">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-2b722924-64d3-4ac8-81cd-389c0281e59b">
                The Deputy Prime Minister (Mr. Attlee)
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-2b722924-64d3-4ac8-81cd-389c0281e59b" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-2b722924-64d3-4ac8-81cd-389c0281e59b">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">No, Sir.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-2b722924-64d3-4ac8-81cd-389c0281e59b" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-2b722924-64d3-4ac8-81cd-389c0281e59b">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-a8355063-99de-49bb-9479-2b86c79d05d2">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-a8355063-99de-49bb-9479-2b86c79d05d2">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-a8355063-99de-49bb-9479-2b86c79d05d2">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-a8355063-99de-49bb-9479-2b86c79d05d2">
                Mr. Granville
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-a8355063-99de-49bb-9479-2b86c79d05d2" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-a8355063-99de-49bb-9479-2b86c79d05d2">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">In view of the fact that every disaster in the war has been preceded by the failure of the Government to secure united strategy, is there any regular consultation with the United Nations?</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-a8355063-99de-49bb-9479-2b86c79d05d2" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-a8355063-99de-49bb-9479-2b86c79d05d2">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-4ed9c3b7-f597-4131-a171-737c71f2e15b">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-4ed9c3b7-f597-4131-a171-737c71f2e15b">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-4ed9c3b7-f597-4131-a171-737c71f2e15b">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-4ed9c3b7-f597-4131-a171-737c71f2e15b">
                Mr. Attlee
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-4ed9c3b7-f597-4131-a171-737c71f2e15b" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-4ed9c3b7-f597-4131-a171-737c71f2e15b">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">I do not accept the hon. Member's statement. There is full consultation.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-4ed9c3b7-f597-4131-a171-737c71f2e15b" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-4ed9c3b7-f597-4131-a171-737c71f2e15b">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>







<div class="content-item other-content">
        <!-- START statement -->
        <div class="statement col-md-9 hs_ColumnNumber content-container " >
                <span data-toggle="tooltip" data-hop-debug-tooltip class="glyphicon glyphicon-info-sign hidden hidden-xs" data-placement="bottom" title=" hs_ColumnNumber "></span>

<p class="hs_ColumnNumber"><span id="2091" class="column-number column-only" data-column-number="2091"></span></p>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            </div>
        <div class="clearfix"></div>
        <!-- END statement -->
</div><div class="content-item" id="contribution-a3730458-acfb-4782-9b43-7a14ea4680a5">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-a3730458-acfb-4782-9b43-7a14ea4680a5">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-a3730458-acfb-4782-9b43-7a14ea4680a5">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-a3730458-acfb-4782-9b43-7a14ea4680a5">
                Mr. Granville
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-a3730458-acfb-4782-9b43-7a14ea4680a5" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-a3730458-acfb-4782-9b43-7a14ea4680a5">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">Will the Government take advantage of the presence of Field Marshal Smuts to have an Allied War Conference in London to discuss the defence of India and other matters?</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-a3730458-acfb-4782-9b43-7a14ea4680a5" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-a3730458-acfb-4782-9b43-7a14ea4680a5">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-c018bbb2-7255-419f-b092-91afd5872364">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-c018bbb2-7255-419f-b092-91afd5872364">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-c018bbb2-7255-419f-b092-91afd5872364">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-c018bbb2-7255-419f-b092-91afd5872364">
                Mr. Attlee
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-c018bbb2-7255-419f-b092-91afd5872364" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-c018bbb2-7255-419f-b092-91afd5872364">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">The hon. Member has not observed that there was a meeting of the Pacific Council yesterday to deal with these matters.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-c018bbb2-7255-419f-b092-91afd5872364" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1942-10-22/debates/997781ce-9025-488c-ac93-d76051463276/Defence#contribution-c018bbb2-7255-419f-b092-91afd5872364">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





                                    </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="divisionListModal">
</div>


<div class="modal fade" id="suggestedEditModal" data-toggle="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Suggest Correction</h4>
        </div>
        <form id="suggestedEditForm">
            <div class="modal-body">
                <div id="info" class="error-message"></div>
                <input type="text" name="debate-section-external-id" id="debate-section-external-id" hidden />
                <input type="text" name="item-external-id" id="item-external-id" hidden />
                <input type="text" name="house" id="house" hidden />
                <input type="text" name="sitting-date" id="sitting-date" hidden />
                <span class="error" aria-live="polite"></span>
                <div class="form-group">
                    <label>Your Details:</label>
                    <input id="commentor-name" type="text" name="name" class="form-control" placeholder="Enter your full name" required maxlength="250" />
                </div>
                <div class="form-group">
                    <input id="commentor-email" type="email" class="form-control" placeholder="Enter your email" required maxlength="250" />
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <label for="comment-content">Enter your suggested correction:</label>
                        <span class="pull-right">
                            <span id="character">0</span>/1000
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <textarea id="comment-content" class="form-control" rows="8" required maxlength="1000"></textarea>
                    </div>
                    <div class="col-md-5">
                        <div class="recaptcha-container">
                            <div class="instruction">
                                Please prove you are not a robot.
                            </div>
                            <div class="g-recaptcha" data-sitekey="6LelpEsUAAAAAGGd13jUXWJzPjehV1Ls9EcMXfWM"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="suggestion-submit-btn" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
</div>


</div>
        </div>
    </div>

    <div class="footer-bumper"></div>
    <p class="visible-xs"></p>
    <div class="footer">
        <footer class="container hidden-xs">
            <div class="text-muted">
                <a target="_blank" href="http://www.parliament.uk/site-information/azindex/">A-Z index</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/glossary/">Glossary</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/contact-us/">Contact us</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/foi/">Freedom of Information</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/data-protection/">Data Protection</a> |
                <span class="hidden-sm">
                    <a target="_blank" href="http://www.parliament.uk/site-information/job-opportunities/">Jobs</a> |
                    <a target="_blank" href="http://www.parliament.uk/site-information/using-this-website/">Using this website</a> |
                </span>
                <a target="_blank" href="http://www.parliament.uk/site-information/copyright/">Copyright</a>
            </div>
        </footer>
        <footer class="container hidden-sm hidden-md hidden-lg">
            <div class="text-muted"><a target="_blank" href="http://www.parliament.uk/site-information/copyright/" target="_blank">&copy; Parliamentary Copyright</a>
            </div>
        </footer>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/lib/jquery-1.11.0.min.js"><\/script>')</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="/assets/js/bundle?v=-FHNkvrFQ2hlBr_TlXZsL32obpR69TeS4hF0fp5QshE1"></script>







</body>
</html>