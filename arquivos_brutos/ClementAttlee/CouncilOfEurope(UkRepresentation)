
<!DOCTYPE html>
<!--[if IE 7]> <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width" />
    
        <!-- Google Tag Manager -->
    <script>
    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
            new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NFVRG43');</script>
    <!-- End Google Tag Manager -->


    <meta name="description" content="Hansard (the Official Report) is the edited verbatim report of proceedings of both the House of Commons and the House of Lords. Daily Debates from Hansard are published on this website the next working day.">

    <link rel="dns-prefetch" href="//ajax.googleapis.com/" />
    <link rel="shortcut icon" href="/Assets/img/favicon.ico" type="image/x-icon" />

        <title>Council Of Europe (Uk Representation) - Hansard</title>
    <link href="/assets/css/bundle?v=iGtf8YJxXRNBeHXf1CZ6ukADmT4hYFCJq0nsETHFFb41" rel="stylesheet"/>

    <script src="/assets/js/modernizr/bundle?v=4Dmh6ovjssOKPw9dZqg0U8FgOCXLJMRqYqL4wv9UEJM1"></script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>

    <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NFVRG43"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<div id="bootstrap-datepicker-container"></div>
<div class="disclaimer" id="data-disclaimer">
    <div class="container">
        <div class="text">
            <strong>Cookies: </strong>We use cookies to give you the best possible experience on our site. By continuing to use the site you
            agree to our use of cookies. <a href="http://www.parliament.uk/site-information/privacy/">Find out more</a>
        </div>
        <button type="submit" class="btn btn-small">OK</button>
    </div>
</div>
<div id="wrapper">
    <header class="header header-secondary">
    <div class="container">
        <a href="https://www.parliament.uk" class="uk-parliament" target="_blank">
            UK Parliament
        </a>
        <div class="links hidden-xs hidden-sm">
            <a target="_blank" href="http://www.parliament.uk/business/">Parliamentary Business</a>
            <a target="_blank" href="http://www.parliament.uk/mps-lords-and-offices/">MPs, Lords and Offices</a>
            <a target="_blank" href="http://www.parliament.uk/about/">About Parliament</a>
            <a target="_blank" href="http://www.parliament.uk/get-involved/">Get Involved</a>
            <a target="_blank" href="http://www.parliament.uk/visiting/">Visit</a>
            <a target="_blank" href="http://www.parliament.uk/education/">Education</a>
        </div>
    </div>
</header>
<header class="header header-primary ">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="text">Menu</span>
                    <span class="close"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <a class="brand hidden-sm hidden-xs" href="/">
                    <div class="identity-text"><strong>Hansard</strong></div>
                </a>
                
                <a class="navbar-brand hidden-md hidden-lg" href="/">
                    <strong>Hansard</strong>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Commons <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/Commons/latestsittingday">Latest Sitting</a></li>
                            <li><a href="/Commons">Browse Sittings</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Debates?house=commons">Find Debates</a></li>
                            <li><a href="/search/Divisions?house=commons">Find Divisions</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Members?house=commons&amp;currentFormerFilter=1">Find MPs</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lords <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/Lords/latestsittingday">Latest Sitting</a></li>
                            <li><a href="/Lords">Browse Sittings</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Debates?house=lords">Find Debates</a></li>
                            <li><a href="/search/Divisions?house=lords">Find Divisions</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Members?house=lords&amp;currentFormerFilter=1">Find Peers</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/about?historic=false">Hansard Online</a></li>
                        </ul>
                    </li>
                </ul>

<form action="/search" class="navbar-form navbar-right" method="post" role="search">                    <div class="search-form">
                        <input id="SearchTermTop" name="SearchTerm" type="text" class="form-control" placeholder="Search Hansard...">
                        <button type="submit" class="btn"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
</form>            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    
    <div class="to-top-link">
        <div id="top-link-block" class="hidden">
            <a href="#" class="well well-sm hidden" id="previous-top-link"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a>
            <a href="#header" class="well well-sm" id="back-to-top-link"><i class="glyphicon glyphicon-chevron-up"></i> Top</a>
            <a href="#" class="well well-sm hidden" id="next-top-link"><i class="glyphicon glyphicon-chevron-right"></i> Next</a>
        </div>
    </div>

</header>


    <!-- Alert area always shown as they are added dynamically via JS -->
<div class="alert-area"></div>


    <div class="">
        <div id="main" class="content">
            

<!-- House coloured header bar -->
<div class="background-commons house-header col-xs-12">
    <div class="container">
        <span>House of Commons <strong>Hansard</strong></span>
        <div class="survey">
    <div class="message">
        <!-- Populated via JavaScript -->
    </div>
    <a href="#" class="close-button" title="Tap to hide survey banner">x</a>
</div>
    </div>
</div>

<div class="">
    

<div class="row content debate-content">
    <div class="col-xs-12">
        <!-- the breadcrumb is to navigate around the day and go back to the date picker-->
        <div class="breadcrumb-container breadcrumb-desktop hidden-xs">
            <div class="container">
                <ol class="breadcrumb">
                    <li>
                        <a href="/Commons/1950-03-16">
                            <strong>Contents</strong>
                        </a>
                    </li>
                        <li>
                                <a href="/Commons/1950-03-16/debates/4aa41d88-b05d-4094-a2dc-f4807cf05acc/CommonsChamber">
                                    <span class="commons">Commons Chamber</span>
                                </a>
                        </li>

                    <li>
                        <div class="hidden-sm download-as-text">
                            <a class="link-text" href="/debates/GetDebateAsText/342ab60a-cf85-462f-9037-3b2eba19a25f"><span class="glyphicon glyphicon-file"></span> Text only</a>
                        </div>

                    </li>
                </ol>
            </div>
        </div>

        <div class="breadcrumb breadcrumb-mobile visible-xs">
            <a class="btn btn-wide" href="/Commons/1950-03-16">
                Back to Contents
            </a>
        </div>

    </div>

    <div class="col-xs-12">
<div class="clearfix"></div>
<div class="alert-area">
    <div id="process-warning" class="alert-outer alert-hidden" data-processing-url="/processing/getcurrentlyprocessing">
    <div class="alert alert-info">
        <div class="alert-inner">
            <span class="glyphicon glyphicon-info-sign"></span>
            <span id="process-warning-message"></span>
        </div>
    </div>
</div>


            <div class="alert-outer">
                <div class="alert alert-info">
                    <div class="alert-inner">
                        <span class="glyphicon glyphicon-info-sign"></span>
                        The text on this page has been created from Hansard archive content, it may contain typographical errors.
                    </div>
                </div>
            </div>
</div>

    </div>

    <div class="col-xs-12 header">
        <div class="container">
            <div class="left-arrow">
                    <a href="/Commons/1950-03-16/debates/94ecdbbc-3708-4c78-ae94-8fc2bf361bf8/BusinessOfTheHouse" class="previous" title="Go to the previous debate">


                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
            </div>

            <div class="title">
                Council Of Europe (Uk Representation)
            </div>

            <div class="right-arrow">
                    <a href="/Commons/1950-03-16/debates/da2a6fa7-edac-4b37-bd78-a0200695d9b5/CouncilOfEurope%E2%80%94Committees" class="next" title="Go to the next debate">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
            </div>
        </div>

            <div class="col-xs-12">
                <div class="actions">
                    <div class="share-debate">
                        <a href="#debate-2982823" rel="popover" class="link-to-contribution link-text" title="Share this debate" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1950-03-16/debates/342ab60a-cf85-462f-9037-3b2eba19a25f/CouncilOfEurope(UkRepresentation)">
                            <div class="share-icon">&nbsp;</div> Share
                        </a>
                    </div>
                </div>
            </div>


</div>


    <div class="col-xs-12 debate-date">
        <strong>16 March 1950</strong>
    </div>

    <div class="col-xs-12 debate-date">Volume 472</div>

    <div class="col-xs-12 col-md-12">
        <div class="container">
            <div id="debateContent">
                <div class="highlightedSearchTerms"></div>
                <div class="content-container">
<div class="content-item" id="contribution-bb8352fe-f45b-441b-bd27-af753bc6cb58">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-bb8352fe-f45b-441b-bd27-af753bc6cb58">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-bb8352fe-f45b-441b-bd27-af753bc6cb58">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-bb8352fe-f45b-441b-bd27-af753bc6cb58">
                The Prime Minister (Mr. Attlee)
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-bb8352fe-f45b-441b-bd27-af753bc6cb58" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1950-03-16/debates/342ab60a-cf85-462f-9037-3b2eba19a25f/CouncilOfEurope(UkRepresentation)#contribution-bb8352fe-f45b-441b-bd27-af753bc6cb58">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">With your permission, Mr. Speaker, I should like to make a short statement on the representation of the United Kingdom in the Consultative Assembly of the Council of Europe.<p></p> Hon. Members will recall that the appointment of United Kingdom delegates to the Assembly was to run either until the beginning of the next session of the Assembly or until the dissolution of the United Kingdom Parliament then sitting, whichever was the earlier. The situation therefore is that at present, in <span id="1260" class="column-number" data-column-number="1260"></span> form, we have no representatives on the Consultative Assembly and a fresh delegation must be appointed before the next session. As that is not likely to take place before the summer, there is no immediate urgency in that problem and I do not propose to deal with it today.<p></p> The immediate difficulty is that committees of the Assembly continue to meet and one of them, the General Affairs Committee, is to meet as soon as next Tuesday. It is clearly essential that the United Kingdom should be represented on these bodies.<p></p> In meeting this situation two points have to be borne in mind; first the rules of the Assembly, as at present constituted, make it impossible for any new delegate to be appointed until there is a session of the Assembly to verify his credentials; secondly, it will doubtless be the desire of this House that our representation on committees should be adjusted to take account of the changed balance of parties here.<p></p> I would therefore propose, subject to the agreement of the Opposition parties, to ask all the existing Conservative members of committees to continue to act, and to invite one out of two of the Labour members to continue on each committee which is expected to meet, thus giving equal representation on the committees to the two main parties. The only exception to this arrangement is the Standing Committee of the Consultative Assembly on which I am inviting all four of the present members to continue to serve, that is two Labour and one Conservative member, and the noble Lord, Lord Layton.<p></p> With permission, I propose to circulate in the OFFICIAL REPORT the names of hon. Members who will be acting as our representatives on these committees.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-bb8352fe-f45b-441b-bd27-af753bc6cb58" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1950-03-16/debates/342ab60a-cf85-462f-9037-3b2eba19a25f/CouncilOfEurope(UkRepresentation)#contribution-bb8352fe-f45b-441b-bd27-af753bc6cb58">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-aaa014c4-5961-4925-8c13-4589424124f4">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-aaa014c4-5961-4925-8c13-4589424124f4">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-aaa014c4-5961-4925-8c13-4589424124f4">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-aaa014c4-5961-4925-8c13-4589424124f4">
                Mr. Harold Macmillan
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-aaa014c4-5961-4925-8c13-4589424124f4" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1950-03-16/debates/342ab60a-cf85-462f-9037-3b2eba19a25f/CouncilOfEurope(UkRepresentation)#contribution-aaa014c4-5961-4925-8c13-4589424124f4">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">While thanking the Prime Minister for this statement, and for saying that this is subject to the agreement of the Opposition parties, perhaps he would allow me to consult with my colleagues before definitely committing myself. We had some unofficial talks late last night and this is in a somewhat different form from what I then understood the statement would be. In order to clarify the situation, do I take it that from a legal point of view, none of us is a member of the Consultative Assembly: we all have to be <span id="1261" class="column-number" data-column-number="1261"></span> re-appointed, but we cannot be reappointed until the next meeting of the Consultative Assembly, which probably will not take place until the summer of this year? Is it then clear that those hon. Members who have survived the election can continue to sit on the committees by the invitation of the Prime Minister without any objection from the managing authorities of the Consultative Assembly? Has that been cleared by the Bureau? It might be argued that, as the Prime Minister said, we are not in form any longer members of the Assembly and could not be members of the committees. Has that been cleared?</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-aaa014c4-5961-4925-8c13-4589424124f4" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1950-03-16/debates/342ab60a-cf85-462f-9037-3b2eba19a25f/CouncilOfEurope(UkRepresentation)#contribution-aaa014c4-5961-4925-8c13-4589424124f4">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-4c9404b7-0067-4880-8a27-1a267b48d875">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-4c9404b7-0067-4880-8a27-1a267b48d875">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-4c9404b7-0067-4880-8a27-1a267b48d875">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-4c9404b7-0067-4880-8a27-1a267b48d875">
                The Prime Minister
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-4c9404b7-0067-4880-8a27-1a267b48d875" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1950-03-16/debates/342ab60a-cf85-462f-9037-3b2eba19a25f/CouncilOfEurope(UkRepresentation)#contribution-4c9404b7-0067-4880-8a27-1a267b48d875">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">Yes, the answer is that that has been cleared with the Bureau.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-4c9404b7-0067-4880-8a27-1a267b48d875" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1950-03-16/debates/342ab60a-cf85-462f-9037-3b2eba19a25f/CouncilOfEurope(UkRepresentation)#contribution-4c9404b7-0067-4880-8a27-1a267b48d875">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-9b2654c4-a03c-4cf2-8f29-027f229f8d0e">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-9b2654c4-a03c-4cf2-8f29-027f229f8d0e">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-9b2654c4-a03c-4cf2-8f29-027f229f8d0e">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-9b2654c4-a03c-4cf2-8f29-027f229f8d0e">
                Mr. Macmillan
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-9b2654c4-a03c-4cf2-8f29-027f229f8d0e" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1950-03-16/debates/342ab60a-cf85-462f-9037-3b2eba19a25f/CouncilOfEurope(UkRepresentation)#contribution-9b2654c4-a03c-4cf2-8f29-027f229f8d0e">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">If that is so—and I am very much relieved to hear it, and I think it is a very sensible arrangement—there can be no re-adjustment of the balance of parties until the new appointments are made for the next meeting of the Assembly. I think this arrangement, subject to consultation with my colleagues, seems a fair attempt on the part of the Government to equalise the representation on the committees, but again, is it within the power of the Government of Great Britain to say to a member who is still now agreed to be a member of the Assembly, that he is not to sit on a committee of which he has been elected by the Assembly, and not by the British Government? That should be cleared. I would point out that in fact all this equalisation of parties boils down to this, that one Socialist representative, Mr. Ungoed-Thomas, is no longer a member of a committee because he is no longer a Member of this House and, either by accident or design, the hon. Member for Reading, North (Mr. R. Mackay) has been—conveniently I think, to the Government—eliminated from the General Affairs Committee.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-9b2654c4-a03c-4cf2-8f29-027f229f8d0e" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1950-03-16/debates/342ab60a-cf85-462f-9037-3b2eba19a25f/CouncilOfEurope(UkRepresentation)#contribution-9b2654c4-a03c-4cf2-8f29-027f229f8d0e">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>







<div class="content-item other-content">
        <!-- START statement -->
        <div class="statement col-md-9  content-container " id='contribution-3a19d064-efb0-4861-9660-e3a6142335c0'>
            
<p class=""><em>Following are the names</em>:</p>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            </div>
        <div class="clearfix"></div>
        <!-- END statement -->
</div>                                    </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="divisionListModal">
</div>


<div class="modal fade" id="suggestedEditModal" data-toggle="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Suggest Correction</h4>
        </div>
        <form id="suggestedEditForm">
            <div class="modal-body">
                <div id="info" class="error-message"></div>
                <input type="text" name="debate-section-external-id" id="debate-section-external-id" hidden />
                <input type="text" name="item-external-id" id="item-external-id" hidden />
                <input type="text" name="house" id="house" hidden />
                <input type="text" name="sitting-date" id="sitting-date" hidden />
                <span class="error" aria-live="polite"></span>
                <div class="form-group">
                    <label>Your Details:</label>
                    <input id="commentor-name" type="text" name="name" class="form-control" placeholder="Enter your full name" required maxlength="250" />
                </div>
                <div class="form-group">
                    <input id="commentor-email" type="email" class="form-control" placeholder="Enter your email" required maxlength="250" />
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <label for="comment-content">Enter your suggested correction:</label>
                        <span class="pull-right">
                            <span id="character">0</span>/1000
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <textarea id="comment-content" class="form-control" rows="8" required maxlength="1000"></textarea>
                    </div>
                    <div class="col-md-5">
                        <div class="recaptcha-container">
                            <div class="instruction">
                                Please prove you are not a robot.
                            </div>
                            <div class="g-recaptcha" data-sitekey="6LelpEsUAAAAAGGd13jUXWJzPjehV1Ls9EcMXfWM"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="suggestion-submit-btn" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
</div>


</div>
        </div>
    </div>

    <div class="footer-bumper"></div>
    <p class="visible-xs"></p>
    <div class="footer">
        <footer class="container hidden-xs">
            <div class="text-muted">
                <a target="_blank" href="http://www.parliament.uk/site-information/azindex/">A-Z index</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/glossary/">Glossary</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/contact-us/">Contact us</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/foi/">Freedom of Information</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/data-protection/">Data Protection</a> |
                <span class="hidden-sm">
                    <a target="_blank" href="http://www.parliament.uk/site-information/job-opportunities/">Jobs</a> |
                    <a target="_blank" href="http://www.parliament.uk/site-information/using-this-website/">Using this website</a> |
                </span>
                <a target="_blank" href="http://www.parliament.uk/site-information/copyright/">Copyright</a>
            </div>
        </footer>
        <footer class="container hidden-sm hidden-md hidden-lg">
            <div class="text-muted"><a target="_blank" href="http://www.parliament.uk/site-information/copyright/" target="_blank">&copy; Parliamentary Copyright</a>
            </div>
        </footer>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/lib/jquery-1.11.0.min.js"><\/script>')</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="/assets/js/bundle?v=-FHNkvrFQ2hlBr_TlXZsL32obpR69TeS4hF0fp5QshE1"></script>







</body>
</html>