
<!DOCTYPE html>
<!--[if IE 7]> <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width" />
    
        <!-- Google Tag Manager -->
    <script>
    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
            new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NFVRG43');</script>
    <!-- End Google Tag Manager -->


    <meta name="description" content="Hansard (the Official Report) is the edited verbatim report of proceedings of both the House of Commons and the House of Lords. Daily Debates from Hansard are published on this website the next working day.">

    <link rel="dns-prefetch" href="//ajax.googleapis.com/" />
    <link rel="shortcut icon" href="/Assets/img/favicon.ico" type="image/x-icon" />

        <title>Official Report (Alterations) - Hansard</title>
    <link href="/assets/css/bundle?v=iGtf8YJxXRNBeHXf1CZ6ukADmT4hYFCJq0nsETHFFb41" rel="stylesheet"/>

    <script src="/assets/js/modernizr/bundle?v=4Dmh6ovjssOKPw9dZqg0U8FgOCXLJMRqYqL4wv9UEJM1"></script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>

    <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NFVRG43"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<div id="bootstrap-datepicker-container"></div>
<div class="disclaimer" id="data-disclaimer">
    <div class="container">
        <div class="text">
            <strong>Cookies: </strong>We use cookies to give you the best possible experience on our site. By continuing to use the site you
            agree to our use of cookies. <a href="http://www.parliament.uk/site-information/privacy/">Find out more</a>
        </div>
        <button type="submit" class="btn btn-small">OK</button>
    </div>
</div>
<div id="wrapper">
    <header class="header header-secondary">
    <div class="container">
        <a href="https://www.parliament.uk" class="uk-parliament" target="_blank">
            UK Parliament
        </a>
        <div class="links hidden-xs hidden-sm">
            <a target="_blank" href="http://www.parliament.uk/business/">Parliamentary Business</a>
            <a target="_blank" href="http://www.parliament.uk/mps-lords-and-offices/">MPs, Lords and Offices</a>
            <a target="_blank" href="http://www.parliament.uk/about/">About Parliament</a>
            <a target="_blank" href="http://www.parliament.uk/get-involved/">Get Involved</a>
            <a target="_blank" href="http://www.parliament.uk/visiting/">Visit</a>
            <a target="_blank" href="http://www.parliament.uk/education/">Education</a>
        </div>
    </div>
</header>
<header class="header header-primary ">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="text">Menu</span>
                    <span class="close"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <a class="brand hidden-sm hidden-xs" href="/">
                    <div class="identity-text"><strong>Hansard</strong></div>
                </a>
                
                <a class="navbar-brand hidden-md hidden-lg" href="/">
                    <strong>Hansard</strong>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Commons <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/Commons/latestsittingday">Latest Sitting</a></li>
                            <li><a href="/Commons">Browse Sittings</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Debates?house=commons">Find Debates</a></li>
                            <li><a href="/search/Divisions?house=commons">Find Divisions</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Members?house=commons&amp;currentFormerFilter=1">Find MPs</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lords <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/Lords/latestsittingday">Latest Sitting</a></li>
                            <li><a href="/Lords">Browse Sittings</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Debates?house=lords">Find Debates</a></li>
                            <li><a href="/search/Divisions?house=lords">Find Divisions</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Members?house=lords&amp;currentFormerFilter=1">Find Peers</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/about?historic=false">Hansard Online</a></li>
                        </ul>
                    </li>
                </ul>

<form action="/search" class="navbar-form navbar-right" method="post" role="search">                    <div class="search-form">
                        <input id="SearchTermTop" name="SearchTerm" type="text" class="form-control" placeholder="Search Hansard...">
                        <button type="submit" class="btn"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
</form>            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    
    <div class="to-top-link">
        <div id="top-link-block" class="hidden">
            <a href="#" class="well well-sm hidden" id="previous-top-link"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a>
            <a href="#header" class="well well-sm" id="back-to-top-link"><i class="glyphicon glyphicon-chevron-up"></i> Top</a>
            <a href="#" class="well well-sm hidden" id="next-top-link"><i class="glyphicon glyphicon-chevron-right"></i> Next</a>
        </div>
    </div>

</header>


    <!-- Alert area always shown as they are added dynamically via JS -->
<div class="alert-area"></div>


    <div class="">
        <div id="main" class="content">
            

<!-- House coloured header bar -->
<div class="background-commons house-header col-xs-12">
    <div class="container">
        <span>House of Commons <strong>Hansard</strong></span>
        <div class="survey">
    <div class="message">
        <!-- Populated via JavaScript -->
    </div>
    <a href="#" class="close-button" title="Tap to hide survey banner">x</a>
</div>
    </div>
</div>

<div class="">
    

<div class="row content debate-content">
    <div class="col-xs-12">
        <!-- the breadcrumb is to navigate around the day and go back to the date picker-->
        <div class="breadcrumb-container breadcrumb-desktop hidden-xs">
            <div class="container">
                <ol class="breadcrumb">
                    <li>
                        <a href="/Commons/1955-02-03">
                            <strong>Contents</strong>
                        </a>
                    </li>
                        <li>
                                <a href="/Commons/1955-02-03/debates/49d33c88-2503-426d-9602-2d63fd45399b/CommonsChamber">
                                    <span class="commons">Commons Chamber</span>
                                </a>
                        </li>

                    <li>
                        <div class="hidden-sm download-as-text">
                            <a class="link-text" href="/debates/GetDebateAsText/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026"><span class="glyphicon glyphicon-file"></span> Text only</a>
                        </div>

                    </li>
                </ol>
            </div>
        </div>

        <div class="breadcrumb breadcrumb-mobile visible-xs">
            <a class="btn btn-wide" href="/Commons/1955-02-03">
                Back to Contents
            </a>
        </div>

    </div>

    <div class="col-xs-12">
<div class="clearfix"></div>
<div class="alert-area">
    <div id="process-warning" class="alert-outer alert-hidden" data-processing-url="/processing/getcurrentlyprocessing">
    <div class="alert alert-info">
        <div class="alert-inner">
            <span class="glyphicon glyphicon-info-sign"></span>
            <span id="process-warning-message"></span>
        </div>
    </div>
</div>


            <div class="alert-outer">
                <div class="alert alert-info">
                    <div class="alert-inner">
                        <span class="glyphicon glyphicon-info-sign"></span>
                        The text on this page has been created from Hansard archive content, it may contain typographical errors.
                    </div>
                </div>
            </div>
</div>

    </div>

    <div class="col-xs-12 header">
        <div class="container">
            <div class="left-arrow">
                    <a href="/Commons/1955-02-03/debates/5b296fd2-d4ec-47ac-8ccf-47516c8eb083/BusinessOfTheHouse" class="previous" title="Go to the previous debate">


                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
            </div>

            <div class="title">
                Official Report (Alterations)
            </div>

            <div class="right-arrow">
                    <a href="/Commons/1955-02-03/debates/7992e4b5-d2d5-447a-a1ea-ba053698f049/RoyalNavy(LivingConditions)" class="next" title="Go to the next debate">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
            </div>
        </div>

            <div class="col-xs-12">
                <div class="actions">
                    <div class="share-debate">
                        <a href="#debate-1801947" rel="popover" class="link-to-contribution link-text" title="Share this debate" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)">
                            <div class="share-icon">&nbsp;</div> Share
                        </a>
                    </div>
                </div>
            </div>


</div>


    <div class="col-xs-12 debate-date">
        <strong>03 February 1955</strong>
    </div>

    <div class="col-xs-12 debate-date">Volume 536</div>

    <div class="col-xs-12 col-md-12">
        <div class="container">
            <div id="debateContent">
                <div class="highlightedSearchTerms"></div>
                <div class="content-container">
<div class="content-item" id="contribution-58651e5c-69e5-4889-8530-579f485c9bf6">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-58651e5c-69e5-4889-8530-579f485c9bf6">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-58651e5c-69e5-4889-8530-579f485c9bf6">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-58651e5c-69e5-4889-8530-579f485c9bf6">
                Mr. Wyatt
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-58651e5c-69e5-4889-8530-579f485c9bf6" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-58651e5c-69e5-4889-8530-579f485c9bf6">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">On a point of order, Mr. Speaker. Yesterday, on Question No. 31, addressed to the Under-Secretary of State for Air, I asked a supplementary question which ran as follows: <blockquote><q>"Can the Under-Secretary say whether the Javelin will be fit for operational service when it is with the squadrons, as it is known that the Hunter is not yet fit for operational service? Would he not agree that the Royal Air Force has no modern fighter fit for operational service at all?"</q></blockquote> It will be within the recollection of the House that the Under-Secretary of State for Air began his reply with the following words: <blockquote><q>"I am dealing with the last point on a later Question. It is not quite true."</q></blockquote> In HANSARD this morning I found that, instead of those words being printed, the following words had been substituted: <blockquote><q>"I am dealing with the last points"—</q></blockquote> in the plural— <span id="1275" class="column-number" data-column-number="1275"></span><blockquote><q>"on a later Question. They are not correct."<span id="vol536_col1083" class="cross-reference" data-house-id="1" data-column-number="1083" data-volume-number="536">—[OFFICIAL REPORT, 2nd February, 1955; Vol. 536, c. 1083.]</q></blockquote> The alteration of the answer to my supplementary question, including the use of the words "They are not correct," is clearly meant to indicate that there was no truth in my suggestion. This is contrary to what the Under-Secretary of State actually said.<p></p> I have inquired of the Editor of HANSARD how this happened. He informed me that a secretary to the Under-Secretary of State for Air had been allowed by the reporter concerned to make the alteration. [HON. MEMBERS: "0h."]</span> He added that, unfortunately, this was not noticed by any of the subeditors. He confirmed that the actual words recorded by the HANSARD reporter were the ones that I have said that the Minister used. I do not blame the reporter; I understand that he was under some pressure. I understand that the Editor of HANSARD proposes to issue an erratum with HANSARD tomorrow.<p></p> I am drawing this matter to your attention, Mr. Speaker, and to that of the House, because it seems to me to raise an important matter of principle. Here is a case in which an official of a Ministry, under the instructions of his Minister, has attempted to water down a significant and damaging admission by that Minister and to make it look like something quite different. May I have your Ruling upon this point, Mr. Speaker?</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-58651e5c-69e5-4889-8530-579f485c9bf6" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-58651e5c-69e5-4889-8530-579f485c9bf6">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-fcc94bea-f470-46ad-bef0-039a2e57c023">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-fcc94bea-f470-46ad-bef0-039a2e57c023">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-fcc94bea-f470-46ad-bef0-039a2e57c023">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-fcc94bea-f470-46ad-bef0-039a2e57c023">
                The Under-Secretary of State for Air (Mr. George Ward)
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-fcc94bea-f470-46ad-bef0-039a2e57c023" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-fcc94bea-f470-46ad-bef0-039a2e57c023">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">I think I can explain this matter quite shortly. The hon. Member for Aston (Mr. Wyatt) asked me a supplementary question yesterday—which he has already quoted. I thought that he was making three quite separate points—the first concerning the Javelin, the second concerning the Hunter, and the third concerning the operational state of Fighter Command. I dealt with the first point, about the Javelin, in my answer, and I meant to indicate that I would deal with the other two points later. As the hon. Member has pointed out, HANSARD quoted it as "point," but it seemed to me that it would be clearer if it read "points." If the hon. Member were going to make only two points and not three, however, he is quite right, and the word should be in the singular, and I have no objection to that.<p></p><span id="1276" class="column-number" data-column-number="1276"></span> The substitution of "not correct" for "not quite true" was an effort to avoid any suggestion of offence to the hon. Member, but I appreciate that a slightly different meaning could be attached to "not correct," and I am quite content for the OFFICIAL REPORT to revert to the original phrase. If the House considers that anything improper has taken place, I take full responsibility and offer my apologies to the House.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-fcc94bea-f470-46ad-bef0-039a2e57c023" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-fcc94bea-f470-46ad-bef0-039a2e57c023">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-6673dbc0-7ffc-45e4-80eb-75735e922564">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-6673dbc0-7ffc-45e4-80eb-75735e922564">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-6673dbc0-7ffc-45e4-80eb-75735e922564">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-6673dbc0-7ffc-45e4-80eb-75735e922564">
                Mr. Speaker
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-6673dbc0-7ffc-45e4-80eb-75735e922564" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-6673dbc0-7ffc-45e4-80eb-75735e922564">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">The hon. Member for Aston (Mr. Wyatt) was good enough to draw my attention to this matter this forenoon, and I have looked into it as well as I have been able to in the time at my disposal. What he says is quite right. The reporter did accept this correction, and I ought to state, for the guidance of the House and everybody else concerned, what is the rule of the House in this matter. Hon. Members are permitted to—and frequently do—correct the transcripts of the reports of their speeches. It is permissible to make a correction which improves the grammar, the syntax or, indeed, the clarity of what was actually said in the House. But let there be no mistake about the rule of the House in these matters. It is not permissible to make alterations in the transcript which materially alter the sense of what was said.<p></p> Having heard both sides in this matter, I think that in this case there was an error. If a reporter is in doubt—and there is sometimes a narrow line between what is an alteration affecting the expression, on the one hand, and an alteration which affects the sense, on the other—it is his duty to consult a sub-editor, or, if necessary, the Editor himself. That is the rule of the House in these cases. I find that the reporter accepted on his own responsibility a correction which was offered to him.<p></p> Perhaps I may explain to the House, in conclusion, that the reporter, who is an old and valued servant of the House, is, unfortunately, absent today, because he has temporarily succumbed to the prevailing malady of influenza. I have no doubt that the germ of that complaint, which must be presumed to have been then incubating within him, affected his judgment in that particular instance.<p></p> I have said what I have said for the clear guidance of the House in these matters. It is a clear rule that is laid <span id="1277" class="column-number" data-column-number="1277"></span> down. I think the House will agree that those who report our debates serve us, on the whole, very well.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-6673dbc0-7ffc-45e4-80eb-75735e922564" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-6673dbc0-7ffc-45e4-80eb-75735e922564">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-4fdd668b-513a-46fb-9c59-8c10f8706360">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-4fdd668b-513a-46fb-9c59-8c10f8706360">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-4fdd668b-513a-46fb-9c59-8c10f8706360">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-4fdd668b-513a-46fb-9c59-8c10f8706360">
                Hon. Members
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-4fdd668b-513a-46fb-9c59-8c10f8706360" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-4fdd668b-513a-46fb-9c59-8c10f8706360">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">Hear, hear.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-4fdd668b-513a-46fb-9c59-8c10f8706360" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-4fdd668b-513a-46fb-9c59-8c10f8706360">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-0533c2a3-0c02-4b83-b62a-8bc114e27b51">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-0533c2a3-0c02-4b83-b62a-8bc114e27b51">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-0533c2a3-0c02-4b83-b62a-8bc114e27b51">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-0533c2a3-0c02-4b83-b62a-8bc114e27b51">
                Mr. Attlee
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-0533c2a3-0c02-4b83-b62a-8bc114e27b51" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-0533c2a3-0c02-4b83-b62a-8bc114e27b51">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">May I take it from that Ruling, Sir, that, if a Minister feels that he has given an incorrect statement in answer to a Question, his proper course would be to get up and make the alteration on the Floor of the House?</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-0533c2a3-0c02-4b83-b62a-8bc114e27b51" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-0533c2a3-0c02-4b83-b62a-8bc114e27b51">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-0a36f193-d2ad-46b5-9661-5f61e8ca5eb4">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-0a36f193-d2ad-46b5-9661-5f61e8ca5eb4">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-0a36f193-d2ad-46b5-9661-5f61e8ca5eb4">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-0a36f193-d2ad-46b5-9661-5f61e8ca5eb4">
                Mr. Speaker
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-0a36f193-d2ad-46b5-9661-5f61e8ca5eb4" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-0a36f193-d2ad-46b5-9661-5f61e8ca5eb4">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">That is so, and I would make it perfectly clear, if I may, that the rule is precisely the same for Ministers as for other hon. Members of the House.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-0a36f193-d2ad-46b5-9661-5f61e8ca5eb4" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1955-02-03/debates/f7e0538a-7f85-4ab2-b9fd-cf48fe8a5026/OfficialReport(Alterations)#contribution-0a36f193-d2ad-46b5-9661-5f61e8ca5eb4">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





                                    </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="divisionListModal">
</div>


<div class="modal fade" id="suggestedEditModal" data-toggle="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Suggest Correction</h4>
        </div>
        <form id="suggestedEditForm">
            <div class="modal-body">
                <div id="info" class="error-message"></div>
                <input type="text" name="debate-section-external-id" id="debate-section-external-id" hidden />
                <input type="text" name="item-external-id" id="item-external-id" hidden />
                <input type="text" name="house" id="house" hidden />
                <input type="text" name="sitting-date" id="sitting-date" hidden />
                <span class="error" aria-live="polite"></span>
                <div class="form-group">
                    <label>Your Details:</label>
                    <input id="commentor-name" type="text" name="name" class="form-control" placeholder="Enter your full name" required maxlength="250" />
                </div>
                <div class="form-group">
                    <input id="commentor-email" type="email" class="form-control" placeholder="Enter your email" required maxlength="250" />
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <label for="comment-content">Enter your suggested correction:</label>
                        <span class="pull-right">
                            <span id="character">0</span>/1000
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <textarea id="comment-content" class="form-control" rows="8" required maxlength="1000"></textarea>
                    </div>
                    <div class="col-md-5">
                        <div class="recaptcha-container">
                            <div class="instruction">
                                Please prove you are not a robot.
                            </div>
                            <div class="g-recaptcha" data-sitekey="6LelpEsUAAAAAGGd13jUXWJzPjehV1Ls9EcMXfWM"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="suggestion-submit-btn" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
</div>


</div>
        </div>
    </div>

    <div class="footer-bumper"></div>
    <p class="visible-xs"></p>
    <div class="footer">
        <footer class="container hidden-xs">
            <div class="text-muted">
                <a target="_blank" href="http://www.parliament.uk/site-information/azindex/">A-Z index</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/glossary/">Glossary</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/contact-us/">Contact us</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/foi/">Freedom of Information</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/data-protection/">Data Protection</a> |
                <span class="hidden-sm">
                    <a target="_blank" href="http://www.parliament.uk/site-information/job-opportunities/">Jobs</a> |
                    <a target="_blank" href="http://www.parliament.uk/site-information/using-this-website/">Using this website</a> |
                </span>
                <a target="_blank" href="http://www.parliament.uk/site-information/copyright/">Copyright</a>
            </div>
        </footer>
        <footer class="container hidden-sm hidden-md hidden-lg">
            <div class="text-muted"><a target="_blank" href="http://www.parliament.uk/site-information/copyright/" target="_blank">&copy; Parliamentary Copyright</a>
            </div>
        </footer>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/lib/jquery-1.11.0.min.js"><\/script>')</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="/assets/js/bundle?v=-FHNkvrFQ2hlBr_TlXZsL32obpR69TeS4hF0fp5QshE1"></script>







</body>
</html>