
<!DOCTYPE html>
<!--[if IE 7]> <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width" />
    
        <!-- Google Tag Manager -->
    <script>
    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
            new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NFVRG43');</script>
    <!-- End Google Tag Manager -->


    <meta name="description" content="Hansard (the Official Report) is the edited verbatim report of proceedings of both the House of Commons and the House of Lords. Daily Debates from Hansard are published on this website the next working day.">

    <link rel="dns-prefetch" href="//ajax.googleapis.com/" />
    <link rel="shortcut icon" href="/Assets/img/favicon.ico" type="image/x-icon" />

        <title>Lotteries, Betting And Gaming (Royal Commission) - Hansard</title>
    <link href="/assets/css/bundle?v=iGtf8YJxXRNBeHXf1CZ6ukADmT4hYFCJq0nsETHFFb41" rel="stylesheet"/>

    <script src="/assets/js/modernizr/bundle?v=4Dmh6ovjssOKPw9dZqg0U8FgOCXLJMRqYqL4wv9UEJM1"></script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>

    <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NFVRG43"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<div id="bootstrap-datepicker-container"></div>
<div class="disclaimer" id="data-disclaimer">
    <div class="container">
        <div class="text">
            <strong>Cookies: </strong>We use cookies to give you the best possible experience on our site. By continuing to use the site you
            agree to our use of cookies. <a href="http://www.parliament.uk/site-information/privacy/">Find out more</a>
        </div>
        <button type="submit" class="btn btn-small">OK</button>
    </div>
</div>
<div id="wrapper">
    <header class="header header-secondary">
    <div class="container">
        <a href="https://www.parliament.uk" class="uk-parliament" target="_blank">
            UK Parliament
        </a>
        <div class="links hidden-xs hidden-sm">
            <a target="_blank" href="http://www.parliament.uk/business/">Parliamentary Business</a>
            <a target="_blank" href="http://www.parliament.uk/mps-lords-and-offices/">MPs, Lords and Offices</a>
            <a target="_blank" href="http://www.parliament.uk/about/">About Parliament</a>
            <a target="_blank" href="http://www.parliament.uk/get-involved/">Get Involved</a>
            <a target="_blank" href="http://www.parliament.uk/visiting/">Visit</a>
            <a target="_blank" href="http://www.parliament.uk/education/">Education</a>
        </div>
    </div>
</header>
<header class="header header-primary ">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="text">Menu</span>
                    <span class="close"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <a class="brand hidden-sm hidden-xs" href="/">
                    <div class="identity-text"><strong>Hansard</strong></div>
                </a>
                
                <a class="navbar-brand hidden-md hidden-lg" href="/">
                    <strong>Hansard</strong>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Commons <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/Commons/latestsittingday">Latest Sitting</a></li>
                            <li><a href="/Commons">Browse Sittings</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Debates?house=commons">Find Debates</a></li>
                            <li><a href="/search/Divisions?house=commons">Find Divisions</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Members?house=commons&amp;currentFormerFilter=1">Find MPs</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lords <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/Lords/latestsittingday">Latest Sitting</a></li>
                            <li><a href="/Lords">Browse Sittings</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Debates?house=lords">Find Debates</a></li>
                            <li><a href="/search/Divisions?house=lords">Find Divisions</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/search/Members?house=lords&amp;currentFormerFilter=1">Find Peers</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/about?historic=false">Hansard Online</a></li>
                        </ul>
                    </li>
                </ul>

<form action="/search" class="navbar-form navbar-right" method="post" role="search">                    <div class="search-form">
                        <input id="SearchTermTop" name="SearchTerm" type="text" class="form-control" placeholder="Search Hansard...">
                        <button type="submit" class="btn"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
</form>            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    
    <div class="to-top-link">
        <div id="top-link-block" class="hidden">
            <a href="#" class="well well-sm hidden" id="previous-top-link"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a>
            <a href="#header" class="well well-sm" id="back-to-top-link"><i class="glyphicon glyphicon-chevron-up"></i> Top</a>
            <a href="#" class="well well-sm hidden" id="next-top-link"><i class="glyphicon glyphicon-chevron-right"></i> Next</a>
        </div>
    </div>

</header>


    <!-- Alert area always shown as they are added dynamically via JS -->
<div class="alert-area"></div>


    <div class="">
        <div id="main" class="content">
            

<!-- House coloured header bar -->
<div class="background-commons house-header col-xs-12">
    <div class="container">
        <span>House of Commons <strong>Hansard</strong></span>
        <div class="survey">
    <div class="message">
        <!-- Populated via JavaScript -->
    </div>
    <a href="#" class="close-button" title="Tap to hide survey banner">x</a>
</div>
    </div>
</div>

<div class="">
    

<div class="row content debate-content">
    <div class="col-xs-12">
        <!-- the breadcrumb is to navigate around the day and go back to the date picker-->
        <div class="breadcrumb-container breadcrumb-desktop hidden-xs">
            <div class="container">
                <ol class="breadcrumb">
                    <li>
                        <a href="/Commons/1949-02-10">
                            <strong>Contents</strong>
                        </a>
                    </li>
                        <li>
                                <a href="/Commons/1949-02-10/debates/edfe74e5-3219-402e-9ffc-db86230893a6/CommonsChamber">
                                    <span class="commons">Commons Chamber</span>
                                </a>
                        </li>
                        <li>
                                <a href="/Commons/1949-02-10/debates/36107d08-1d56-42e1-9a5a-181f714de9a1/OralAnswersToQuestions">
                                    <span class="commons">Oral Answers To Questions</span>
                                </a>
                        </li>

                    <li>
                        <div class="hidden-sm download-as-text">
                            <a class="link-text" href="/debates/GetDebateAsText/b7143e46-97af-4072-b538-8e9fbf88da83"><span class="glyphicon glyphicon-file"></span> Text only</a>
                        </div>

                    </li>
                </ol>
            </div>
        </div>

        <div class="breadcrumb breadcrumb-mobile visible-xs">
            <a class="btn btn-wide" href="/Commons/1949-02-10">
                Back to Contents
            </a>
        </div>

    </div>

    <div class="col-xs-12">
<div class="clearfix"></div>
<div class="alert-area">
    <div id="process-warning" class="alert-outer alert-hidden" data-processing-url="/processing/getcurrentlyprocessing">
    <div class="alert alert-info">
        <div class="alert-inner">
            <span class="glyphicon glyphicon-info-sign"></span>
            <span id="process-warning-message"></span>
        </div>
    </div>
</div>


            <div class="alert-outer">
                <div class="alert alert-info">
                    <div class="alert-inner">
                        <span class="glyphicon glyphicon-info-sign"></span>
                        The text on this page has been created from Hansard archive content, it may contain typographical errors.
                    </div>
                </div>
            </div>
</div>

    </div>

    <div class="col-xs-12 header">
        <div class="container">
            <div class="left-arrow">
                    <a href="/Commons/1949-02-10/debates/9af27dd3-e00d-4850-bb7b-4ead999aea08/Palestinians(VisitsToUk)" class="previous" title="Go to the previous debate">


                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
            </div>

            <div class="title">
                Lotteries, Betting And Gaming (Royal Commission)
            </div>

            <div class="right-arrow">
                    <a href="/Commons/1949-02-10/debates/b82d957a-60b0-47fd-a3bb-35602478eddb/FestivalOfBritain1951" class="next" title="Go to the next debate">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
            </div>
        </div>

            <div class="col-xs-12">
                <div class="actions">
                    <div class="share-debate">
                        <a href="#debate-2815228" rel="popover" class="link-to-contribution link-text" title="Share this debate" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1949-02-10/debates/b7143e46-97af-4072-b538-8e9fbf88da83/LotteriesBettingAndGaming(RoyalCommission)">
                            <div class="share-icon">&nbsp;</div> Share
                        </a>
                    </div>
                </div>
            </div>


</div>


    <div class="col-xs-12 debate-date">
        <strong>10 February 1949</strong>
    </div>

    <div class="col-xs-12 debate-date">Volume 461</div>

    <div class="col-xs-12 col-md-12">
        <div class="container">
            <div id="debateContent">
                <div class="highlightedSearchTerms"></div>
                <div class="content-container">


<div class="content-item other-content">
        <!-- START statement -->
        <div class="statement col-md-9  content-container " id='contribution-9c4c87dd-b0ae-4f6a-8b79-1f7ba622db25'>
            
<p class="">45.</p>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            </div>
        <div class="clearfix"></div>
        <!-- END statement -->
</div><div class="content-item" id="contribution-5117f309-3f30-4fa2-8ce5-cb7689e3957a">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-5117f309-3f30-4fa2-8ce5-cb7689e3957a">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-5117f309-3f30-4fa2-8ce5-cb7689e3957a">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-5117f309-3f30-4fa2-8ce5-cb7689e3957a">
                Mr. Nally
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-5117f309-3f30-4fa2-8ce5-cb7689e3957a" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1949-02-10/debates/b7143e46-97af-4072-b538-8e9fbf88da83/LotteriesBettingAndGaming(RoyalCommission)#contribution-5117f309-3f30-4fa2-8ce5-cb7689e3957a">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">asked the Prime Minister whether he will now make a statement with regard to the proposal to recommend to His Majesty the setting up of a Royal Commission on Lotteries, Betting and Gaming.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-5117f309-3f30-4fa2-8ce5-cb7689e3957a" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1949-02-10/debates/b7143e46-97af-4072-b538-8e9fbf88da83/LotteriesBettingAndGaming(RoyalCommission)#contribution-5117f309-3f30-4fa2-8ce5-cb7689e3957a">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-c9d5a167-2feb-4820-8861-5cc6d42998fa">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-c9d5a167-2feb-4820-8861-5cc6d42998fa">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-c9d5a167-2feb-4820-8861-5cc6d42998fa">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-c9d5a167-2feb-4820-8861-5cc6d42998fa">
                The Prime Minister (Mr. Attlee)
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-c9d5a167-2feb-4820-8861-5cc6d42998fa" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1949-02-10/debates/b7143e46-97af-4072-b538-8e9fbf88da83/LotteriesBettingAndGaming(RoyalCommission)#contribution-c9d5a167-2feb-4820-8861-5cc6d42998fa">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">Yes, Sir. The King has been pleased to approve the setting up of a Royal Commission on Lotteries, Betting and Gaming with the following Terms of Reference: <blockquote><q>"To inquire into the existing law and practice thereunder relating to lotteries, betting and gaming, with particular reference to the developments which have taken place since the report of the Royal Commission on Lotteries and Betting in 1933, and to report what changes, if any, are desirable and practicable."</q></blockquote> I am glad to be able to announce that the right hon. H. U. Willink, K.C., M.C., Master of Magdalene College, Cambridge, will act as Chairman. I am not yet in a position to announce the names of the other members of the Commission.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-c9d5a167-2feb-4820-8861-5cc6d42998fa" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1949-02-10/debates/b7143e46-97af-4072-b538-8e9fbf88da83/LotteriesBettingAndGaming(RoyalCommission)#contribution-c9d5a167-2feb-4820-8861-5cc6d42998fa">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-02ebdcc2-81eb-4da6-b67a-e7fcae282df8">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-02ebdcc2-81eb-4da6-b67a-e7fcae282df8">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-02ebdcc2-81eb-4da6-b67a-e7fcae282df8">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-02ebdcc2-81eb-4da6-b67a-e7fcae282df8">
                Mr. Nally
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-02ebdcc2-81eb-4da6-b67a-e7fcae282df8" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1949-02-10/debates/b7143e46-97af-4072-b538-8e9fbf88da83/LotteriesBettingAndGaming(RoyalCommission)#contribution-02ebdcc2-81eb-4da6-b67a-e7fcae282df8">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">While extending my heartiest thanks for the Prime Minister's rather delayed reply, may I ask him whether, pending the results of the work of the Royal Commission, which must necessarily be protracted, he will give an assurance that the existence of the Royal Commission will not necessarily impede action by the Government to deal with gross cases of profiteering, waste of labour and materials and other malpractices in betting and gambling, convincing evidence of which a group of hon. Members and myself propose to present to the Prime Minister at a very early date?</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-02ebdcc2-81eb-4da6-b67a-e7fcae282df8" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1949-02-10/debates/b7143e46-97af-4072-b538-8e9fbf88da83/LotteriesBettingAndGaming(RoyalCommission)#contribution-02ebdcc2-81eb-4da6-b67a-e7fcae282df8">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





<div class="content-item" id="contribution-ded350b2-3798-42d0-be93-b1ccc48ed67b">
    <div class="col-md-9 edit-fail-error">
        <div class="alert alert-danger" id="alert-ded350b2-3798-42d0-be93-b1ccc48ed67b">
            <span class="glyphicon glyphicon-info-sign"></span>The edit just sent has not been saved.  The following error was returned:
        </div>
    </div>
    <div class="col-md-9 multiple-edit-warning">
        <div class="alert alert-warning" id="warning-ded350b2-3798-42d0-be93-b1ccc48ed67b">
            <span class="glyphicon glyphicon-info-sign"></span>This content has already been edited and is awaiting review.
        </div>
    </div>

        <div class="col-md-9 nohighlight member-container">
            <h2 class="memberLink col-md-9" id="member-link-ded350b2-3798-42d0-be93-b1ccc48ed67b">
                The Prime Minister
            </h2>
        </div>
            <div class="col-md-3 hidden-sm hidden-xs right-column ">
            <a href="#contribution-ded350b2-3798-42d0-be93-b1ccc48ed67b" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
               data-hop-popover="http://hansard.parliament.uk/Commons/1949-02-10/debates/b7143e46-97af-4072-b538-8e9fbf88da83/LotteriesBettingAndGaming(RoyalCommission)#contribution-ded350b2-3798-42d0-be93-b1ccc48ed67b">
                <div class="share-icon">&nbsp;</div><span class="share-text">Share</span>
            </a>
        </div>

    


    <div class="inner">
        <div class="col-md-9 contribution content-container">



<p class="">I should like to wait and see that.</p>                    <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                        <a href="#contribution-ded350b2-3798-42d0-be93-b1ccc48ed67b" rel="popover" class="link-to-contribution link-to-contribution-mobile link-text" title="Share this contribution" data-placement="top"
                           data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                           data-hop-popover="http://hansard.parliament.uk/Commons/1949-02-10/debates/b7143e46-97af-4072-b538-8e9fbf88da83/LotteriesBettingAndGaming(RoyalCommission)#contribution-ded350b2-3798-42d0-be93-b1ccc48ed67b">
                            <div class="share-icon">&nbsp;</div>&nbsp;Share
                        </a>
                    </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>





                                    </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="divisionListModal">
</div>


<div class="modal fade" id="suggestedEditModal" data-toggle="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Suggest Correction</h4>
        </div>
        <form id="suggestedEditForm">
            <div class="modal-body">
                <div id="info" class="error-message"></div>
                <input type="text" name="debate-section-external-id" id="debate-section-external-id" hidden />
                <input type="text" name="item-external-id" id="item-external-id" hidden />
                <input type="text" name="house" id="house" hidden />
                <input type="text" name="sitting-date" id="sitting-date" hidden />
                <span class="error" aria-live="polite"></span>
                <div class="form-group">
                    <label>Your Details:</label>
                    <input id="commentor-name" type="text" name="name" class="form-control" placeholder="Enter your full name" required maxlength="250" />
                </div>
                <div class="form-group">
                    <input id="commentor-email" type="email" class="form-control" placeholder="Enter your email" required maxlength="250" />
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <label for="comment-content">Enter your suggested correction:</label>
                        <span class="pull-right">
                            <span id="character">0</span>/1000
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <textarea id="comment-content" class="form-control" rows="8" required maxlength="1000"></textarea>
                    </div>
                    <div class="col-md-5">
                        <div class="recaptcha-container">
                            <div class="instruction">
                                Please prove you are not a robot.
                            </div>
                            <div class="g-recaptcha" data-sitekey="6LelpEsUAAAAAGGd13jUXWJzPjehV1Ls9EcMXfWM"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="suggestion-submit-btn" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
</div>


</div>
        </div>
    </div>

    <div class="footer-bumper"></div>
    <p class="visible-xs"></p>
    <div class="footer">
        <footer class="container hidden-xs">
            <div class="text-muted">
                <a target="_blank" href="http://www.parliament.uk/site-information/azindex/">A-Z index</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/glossary/">Glossary</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/contact-us/">Contact us</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/foi/">Freedom of Information</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/data-protection/">Data Protection</a> |
                <span class="hidden-sm">
                    <a target="_blank" href="http://www.parliament.uk/site-information/job-opportunities/">Jobs</a> |
                    <a target="_blank" href="http://www.parliament.uk/site-information/using-this-website/">Using this website</a> |
                </span>
                <a target="_blank" href="http://www.parliament.uk/site-information/copyright/">Copyright</a>
            </div>
        </footer>
        <footer class="container hidden-sm hidden-md hidden-lg">
            <div class="text-muted"><a target="_blank" href="http://www.parliament.uk/site-information/copyright/" target="_blank">&copy; Parliamentary Copyright</a>
            </div>
        </footer>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/lib/jquery-1.11.0.min.js"><\/script>')</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="/assets/js/bundle?v=-FHNkvrFQ2hlBr_TlXZsL32obpR69TeS4hF0fp5QshE1"></script>







</body>
</html>