from bs4 import BeautifulSoup
from datetime import datetime


class Pronunciamento:
    def __init__(self, url, ministro="Attlee"):
        with open(url, encoding="utf8", errors="ignore") as f:
            self.ministro = ministro
            self.soup = BeautifulSoup(f, "lxml")
            self.data = self.get_data()
            self.titulo = self.get_titulo().strip()
            self.pronunciamentos = self.get_pronunciamentos()
            self.camara = self.get_camara()
            self.data_obj = datetime.fromisoformat(self.data)

    def __str__(self):
        return f"Titulo: {self.titulo} \n Camara: {self.camara} \n Data: {self.data} \n falas: {self.pronunciamentos}"

    def get_data(self):
        data = self.soup.find("div", class_="col-xs-12 debate-date")
        data = data.find("strong").get_text()
        dia, mes, ano = data.split()
        meses = {
            "Jan": "01",
            "Feb": "02",
            "Mar": "03",
            "Apr": "04",
            "May": "05",
            "Jun": "06",
            "Jul": "07",
            "Aug": "08",
            "Sep": "09",
            "Oct": "10",
            "Nov": "11",
            "Dec": "12",
            "January": "01",
            "February": "02",
            "March": "03",
            "April": "04",
            "June": "06",
            "July": "07",
            "August": "08",
            "September": "09",
            "October": "10",
            "November": "11",
            "December": "12",
        }
        data_formatada = str(f"{ano}-{meses[mes]}-{dia}")
        return data_formatada

    def get_titulo(self):
        titulo = self.soup.find("div", class_="title")
        titulo = titulo.get_text().strip(" ").strip("  ").strip("\n")
        return titulo

    def get_camara(self):
        camara = self.soup.find("div", class_="house-header").find("span").get_text()

        return camara

    def get_pronunciamentos(self):
        list_pronunciamentos = []
        pronunciamentos = self.soup.find_all("div", class_="content-item")
        for fala in pronunciamentos:
            try:
                fala_autor = fala.find("div", class_="col-md-9 nohighlight member-container").get_text()
                fala_autor = fala_autor.strip("\n").strip()
                fala_texto = fala.find_all("p")
                list_pronunciamentos.append((fala_autor, fala_texto))
            except AttributeError:
                continue
        return list_pronunciamentos

    def limpa_pronunciamentos(self):
        for fala in self.pronunciamentos:
            autor = fala[0]
            texto = fala[1]
            texto = texto[0].get_text()
            if autor == "MR. J. CHAMBERLAIN" or autor == "Mr. Ronald Chamberlain":
                continue
            else:
                if self.ministro == "Churchill":
                    lista_autores = Pronunciamento.lista_Churchill()
                elif self.ministro == "Attlee":
                    
                    lista_autores = Pronunciamento.lista_Attlee()
                elif self.ministro == "Chamberlain":
                    lista_autores = Pronunciamento.lista_Chamberlain()
            if autor in lista_autores:
                
                self.cria_txt(autor, texto)
                self.insere_na_tabela(autor, texto)
            else:
                continue

    def insere_na_tabela(self, autor, texto):

        arquivo = f"tabela_{self.ministro}.csv"
        ministro = self.ministro

        if autor == "The Prime Minister":
            data_Chamberlain = datetime.fromisoformat('1940-05-10')
            data_inicio_Attlee = datetime.fromisoformat('1945-07-28')
            data_fim_Attle = datetime.fromisoformat('1951-10-26')
            if self.data_obj <= data_Chamberlain:
                arquivo = "tabela_Chamberlain.csv"
                ministro = "Charberlain"
            elif self.data_obj >= data_inicio_Attlee and self.data_obj <= data_fim_Attle:
                arquivo = "tabela_Attlee.csv"
                ministro = "Attlee"
            else:
                arquivo = "tabela_Churchill.csv"
                ministro = "Churchill"

        if ministro == self.ministro:
            nome_arquivo = f"{self.data}-{ministro}-{self.camara}-{self.titulo}-{autor}.txt"
            local_arquivo = f"/home/labri_pedro/prime_minister/arquivos_limpos/{nome_arquivo}"
            with open(arquivo, "a") as file:
                
                file.write(f"{local_arquivo};{nome_arquivo};{self.data};{self.titulo};{autor};{texto}\n")

    def cria_txt(self, autor, texto):
        nome_arquivo = f"{self.data}-{self.ministro}-{self.camara}-{self.titulo}-{autor}.txt"
        arquivo = f".//arquivos_limpos//{self.ministro}//{nome_arquivo}"
        ministro = self.ministro

        if autor == "The Prime Minister":
            data_Chamberlain = datetime.fromisoformat('1940-05-10')
            data_inicio_Attlee = datetime.fromisoformat('1945-07-28')
            data_fim_Attle = datetime.fromisoformat('1951-10-26')
            if self.data_obj <= data_Chamberlain:
                # arquivo = f".//arquivos_limpos//Chamberlain//{nome_arquivo}"
                ministro = "Chamberlain"
            elif self.data_obj >= data_inicio_Attlee and self.data_obj <= data_fim_Attle:
                # arquivo = f".//arquivos_limpos//Attlee//{nome_arquivo}"
                ministro = "Chamberlain"
            else:
                # arquivo = f".//arquivos_limpos//Churchill//{nome_arquivo}"
                ministro = "Churchill"

        if ministro == self.ministro:
            with open(arquivo, "a") as file:
                file.write(texto)
                file.write("\n")

    @staticmethod
    def lista_autores():
        return [
            "Mr. Chamberlain",
            "The Prime Minister (Mr. Chamberlain)",
            "The Prime Minister (Sir Winston Churchill)",
            "The Prime Minister",
            "Mr. Churchill",
            "Mr. Churchill (Woodford)",
            "The Prime Minister (Mr. Churchill)",
            "The First Lord of the Admiralty (Mr. Churchill)",
            "Mr. Attlee",
            "Mr. Attlee (by Private Notice)",
            "Mr. C. R. Attlee",
            "Mr. C. R. Attlee (Walthamstow, West)",
            "The Prime Minister (Mr. Attlee)",
            "The Deputy Prime Minister (Mr. Attlee)",
            "The Lord President of the Council (Mr. Attlee)",
            "The Secretary of State for Dominion Affairs (Mr. Attlee)",
            "The Lord Privy Seal (Mr. Attlee)",
            "The Prime Minister",
        ]

    @staticmethod
    def lista_Attlee():
        return [
            "Mr. Attlee",
            "Mr. Attlee (by Private Notice)",
            "Mr. C. R. Attlee",
            "Mr. C. R. Attlee (Walthamstow, West)",
            "The Prime Minister (Mr. Attlee)",
            "The Deputy Prime Minister (Mr. Attlee)",
            "The Lord President of the Council (Mr. Attlee)",
            "The Secretary of State for Dominion Affairs (Mr. Attlee)",
            "The Lord Privy Seal (Mr. Attlee)",
            "The Prime Minister",
            
        ]

    @staticmethod
    def lista_Chamberlain():
        return ["Mr. Chamberlain", "The Prime Minister (Mr. Chamberlain)", "The Prime Minister"]

    @staticmethod
    def lista_Churchill():
        return [
            "Mr. Churchill",
            "Mr. Churchill (Woodford)",
            "The Prime Minister (Mr. Churchill)",
            "The First Lord of the Admiralty (Mr. Churchill)",
            "The Prime Minister",
        ]
