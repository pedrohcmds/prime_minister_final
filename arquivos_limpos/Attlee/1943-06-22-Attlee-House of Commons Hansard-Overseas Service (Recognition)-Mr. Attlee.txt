I am sure my hon. Friend knows very well the difficulty of settling  these things, with a war going on in various theatres. It is not an easy matter; my hon. Friend knows that well.
I am aware that it was done in the last war, but my hon. Friend will realise the great sense of injustice that may be caused if someone is left out.
