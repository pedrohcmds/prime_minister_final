It is the practice for St. George's Cross to be flown from many parish churches on St. George's Day, and there is nothing to prevent private persons flying flags if they so wish.
