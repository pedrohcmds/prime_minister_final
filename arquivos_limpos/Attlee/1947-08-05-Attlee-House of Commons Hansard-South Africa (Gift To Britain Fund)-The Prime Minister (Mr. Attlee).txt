I have received an interim report from the Committee set up on the Gift to Britain Fund, and I hope to be able to make a definite statement on the allocation after the Recess.
