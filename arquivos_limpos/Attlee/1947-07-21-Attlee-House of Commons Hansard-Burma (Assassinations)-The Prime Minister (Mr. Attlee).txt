I have nothing to add to the very full reports that have appeared in the Press. We are in the closest touch with the Governor, and any information received from him will at once be made available.
