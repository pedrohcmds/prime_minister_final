I have responsibility only for this war. I cannot say why the same course was not adopted in the last war.
I have stated that, in the opinion of His Majesty's Government, they would be of service to the enemy.
