I think the answer I have given shows that the hon. and gallant Gentleman is mistaken in imagining that the provision of these houses is just emergency work, to be swept away at the end of the war. I do not think it is possible to disregard local authorities in putting up local buildings.
That is another question; perhaps my hon. Friend will put it down.
I am not aware of what the hon. Gentleman is referring to; I have not been in the Library lately.
My hon. and gallant Friend may rest assured that, in framing our plans for post-war reconstruction, we are keeping prominently in mind the importance of making as simple and straightforward as possible the procedure for enabling local authorities to obtain the guidance and assistance which they require from the central Government in discharging the duties laid on them by Parliament in this connection.
I hope so, but the hon. and gallant Gentleman knows the difficulties of making legal language intelligible to the layman.
