Perhaps my hon. Friends would be good enough to await the statement which it is proposed to make on the next Sitting Day.
Questions are already on the Paper. It is not possible to give a full answer at Question time. A full statement will be made in the course of the Debate.
