I think my hon. and gallant Friend will realise that my answer, however colourless, gave a perfectly clear indication that we had no intention of returning to the pre-war position.
