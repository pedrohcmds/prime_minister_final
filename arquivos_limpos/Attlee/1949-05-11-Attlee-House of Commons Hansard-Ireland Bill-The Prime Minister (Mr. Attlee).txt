I have it in command from the King to acquaint the House that His Majesty places his Prerogative and Interests so far as concerns the matters dealt with by the Bill, at the disposal of Parliament.
