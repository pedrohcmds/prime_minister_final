May I ask the right hon. Gentleman whether he sees any possibility of dealing with the urgent matter of Indo-China in advance of Korea, with a view to getting some kind of standstill arrangement?
