I would refer the hon. Member to the reply given by my right hon. Friend the Prime Minister to the hon. Member for Eye (Mr. Granville) on 8th December, when he said that he was satisfied that everything possible was being done to make consultation between this country and the Soviet Union as intimate and as constant as possible.
I think my hon. Friend will realise that that matter was very fully explained.
There is liaison, but I do not know what my hon. and gallant Friend means by "serving."
