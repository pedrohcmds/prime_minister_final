I do not think the hon. Member is correct. I have referred him to the full reply that the Prime Minister gave to the hon. Member for Leigh (Mr. Tinker). There are many great difficulties.
I am aware that there is natural feeling about this, but the difficulty is really due to operational conditions.
I will certainly look into it as fully as possible, and I shall be glad to have any information hon. Members can give me.
