Yes, Sir. Under Section 31 of the Militia Act, 1882, where there is no Lieutenant of a  county, His Majesty may authorise any three Deputy Lieutenants of such county to act as the Lieutenant thereof.
