The officers to whom my hon. Friend refers are no doubt those retired officers of the Services who are employed as military commentators by some newspapers. As retired officers they have no access to official plans and papers. Breaches of security will be dealt with in the normal way.
I do not think that in this case any opinions are formed for the enemy which he could not have formed for himself.
I think that that is so, and if there was any exception in any way, some action would be taken.
