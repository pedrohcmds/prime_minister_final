There is at present no vacancy on the Forestry Commission, but I understand that my hon. Friend the Member for the Forest of Dean (Mr. Price) was asked some time ago by the Commission to take a special interest on their behalf in Welsh aspects of their work.
That is a point that might be borne in mind when there is a first vacancy.
