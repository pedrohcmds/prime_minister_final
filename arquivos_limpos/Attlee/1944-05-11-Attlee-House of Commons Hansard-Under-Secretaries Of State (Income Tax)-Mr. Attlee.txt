I would refer my hon. Friend to the replies given on 6th July and 23rd September last to my hon. Friend the Member for South Croydon (Sir H. Williams), to which I have nothing to add at this juncture.
The matter was very fully considered by the late Chancellor of the Exchequer, in consultation with the Under-Secretaries, and a decision was made that the matter had better be left where it is.
The position is not exactly as stated by the hon. Member, but, under the rules and regulations—I am not familiar with the details—it is not open to them, when they are Ministers, to claim these expenses as Members of Parliament.
Perhaps the hon. Member will put that Question down.
The salaries of Under-Secretaries were settled by—[Interruption]—1 will, however, look further into this matter.
