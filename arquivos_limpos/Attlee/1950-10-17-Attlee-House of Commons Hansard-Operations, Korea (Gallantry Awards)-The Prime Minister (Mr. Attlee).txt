Members of British Forces serving in or over Korea or Korean waters will be eligible for all British awards which may be granted for gallantry in operations.
