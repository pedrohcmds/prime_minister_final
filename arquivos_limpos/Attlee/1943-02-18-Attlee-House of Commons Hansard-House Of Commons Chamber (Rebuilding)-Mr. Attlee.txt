Preliminary information has been collected by the Ministry of Works with a view to the preparation of plans and the starting of the work when times are opportune. As has already been announced, the House will be consulted before final plans are made, and all suggestions made by hon. Members will be borne in mind.
I am afraid I cannot do so at the moment.
I will consider that suggestion. No decision has been taken to that effect.
